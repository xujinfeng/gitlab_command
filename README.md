**1.生成ssh秘钥

配置用户名**


`git config --global user.name "$$$$"`

配置邮箱
`git config --global user.email "####@qq.com"`

配置成功后可以生成公钥与秘钥
执行ssh-keygen -t rsa -C "####@qq.com"
按3次Enter，



**上传的项目**
1.打开终端,cd 到你需要上传的项目中

`$ cd /Users/dms/Desktop/项目名称`

2.执行下面的命令行

`$ git config – global user.name "你的用户名,例如:xiaowang"`

$ git config – global user.email "你的邮箱,例如:123321@qq.com,我用的是当前Git账号关联的邮箱"

$ git init

$ git remote add origin 你刚才建立的项目连接 

$ git add .

$ git commit -m “commetn” 

$ git push -u origin master 




////////代码更新/////////////

打开 Mac 终端，切换到本地代码目录，然后查看状态

`  git status`

在终端上输入命令，把更新代码提交上去

`  git add *`

在终端上输入命令，填写修改代码的原由

`  git commit -m '更新原因'`
  
在终端输入命令，拉取当前仓位的最新代码 

`  git pull --rebase origin master`

接着，在终端输入命令，把本地的代码推送到远程代码仓上

`  git push origin `
  
  
**//////////////clone到本地///////////////////**

`git clone ssh 地址`
  



**///////////////////小问题////////////////////////**
1.传上去的文件只有 README.md 
解决方法： 输入git add .
git 空格 add 空格 . （不要少打空格了）

2.错误提示 error: failed to push some refs to 
解决办法 git pull --rebase origin master

3.错误提示 error: cannot pull with rebase: Your index contains uncommitted changes.
解决办法 git commit -m "xx"

4.git push报错error: failed to push some refs to 'git@github.com:
原因： 
GitHub远程仓库中的README.md文件不在本地仓库中。 
解决方案：

$ git pull --rebase origin master
$ git push -u origin master



========================>git 常用操作<================================
git 常用操作 本地版本 git init, 初始化git仓库

git add . 添加到临时仓库

git commit -m 'fix: 36878 update', git 提交本地版本

git commit -a -m <提交信息>跳过add .暂存环节, 直接提交版本

git diff,检查已暂存以及尚未暂存的更改

git diff --cached,而该命令可以显示已暂存的更改与最后一次commit的差异

连接远程仓库 git remote add origin <HTTPS地址或ssh地址>, 添加远程仓库

git remote -v,查看远程仓库

git clone <HTTPS地址或ssh地址>,克隆远程仓库

git pull, 默认拉取远程仓库

git push,推送到远程仓库

分支管理 git branch,查看本地分支

git branch -a，列出所有分支名称

git checkout <分支名>,切换分支

git checkout -b <分支名>,创建并切换分支

git merge dev,合并分支,先切换到master, 再把dev合并到master

git merge --no-ff dev,它会禁用fast forward，使得日志中显示分支的merge信息。

git cherry-pick <commit 号>,合并部分分支

切换远程分支 git checkout -b dev origin/dev，作用是checkout远程的dev分支，在本地起名为dev分支，并切换到本地的dev分支

删除分支 git branch -d <要删除的分支名>,删除本地分支

git branch -D <要删除的分支名>,强制删除

git push origin :,删除远程分支

日志查询 git log -,num表示日志的数量

图形化查看日志 gitk

/////
gitlab 切换远端分支

git remote set-url origin 仓库地址
